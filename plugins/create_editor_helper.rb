# frozen_string_literal: true

require_relative '../lib/create_editor_helper'

Gitlab::Triage::Resource::Context.include CreateEditorHelper

## What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

## Expected impact & dry-runs 

_These are strongly recommended to assist reviewers and reduce the time to merge your change._

See https://gitlab.com/gitlab-org/quality/triage-ops/-/tree/master/doc/scheduled#testing-with-a-dry-run on how to perform dry-runs.

## Action items

* [ ] (If applicable) Add documentation to the handbook pages for [Triage Operations](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/handbook/engineering/quality/triage-operations/index.html.md) =>
* (If applicable) Identify the affected groups and how to communicate to them:
  * [ ] /cc @`person_or_group` =>
  * [ ] Relevant Slack channels =>
  * [ ] Engineering week-in-review

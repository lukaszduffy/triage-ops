# frozen_string_literal: true

require_relative '../triage/processor'
require_relative 'label_jihu_contribution'

module Triage
  class AppSecProcessor < Processor
    APPSEC_APPROVAL_LABEL = 'sec-planning::complete'
    APPSEC_PENDING_APPROVAL_LABEL = 'sec-planning::pending-followup'

    def applicable?
      event.from_gitlab_org? &&
        (has_jihu_contribution_label? || event.jihu_contributor?)
    end

    private

    def approved_by_appsec?(user_id = event.event_actor_id)
      Triage.gitlab_com_appsec_member_ids.include?(user_id)
    end

    def has_appsec_approval_label?
      event.label_names.include?(APPSEC_APPROVAL_LABEL)
    end

    def has_jihu_contribution_label?
      event.label_names.include?(LabelJiHuContribution::LABEL_NAME)
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(unique_comment_key, event)
    end

    def appsec_discussion_thread
      @appsec_discussion_thread ||= unique_comment.previous_discussion
    end

    def unique_comment_key
      # To make this backward compatible, we use this name.
      # At some point if we're ok to break compatibility, we can use
      # 'Triage::AppSecProcessor'
      'Triage::PingAppSecOnApproval'
      # Note that this should not be `self.class.name` because it'll be
      # dynamic for the subclasses.
    end
  end
end

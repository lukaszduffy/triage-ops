# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/processor'
require_relative '../triage/rate_limit'

require 'digest'

module Triage
  class ReactiveLabeler < Processor
    include RateLimit

    LABELS_REGEX = /~"([^"]+)"|~([^ ]+)/.freeze
    ALLOWED_LABELS_REGEX = /\Agroup::[^:]+\z/.freeze

    react_to 'issue.note', 'merge_request.note'
    define_command name: 'label', args_regex: LABELS_REGEX

    def applicable?
      event.from_gitlab_org? &&  # Check this first for code clarity
        event.by_noteable_author? && # Fast and won't pass for most of time
        command.valid?(event) &&
        any_labels_to_apply?
    end

    def process
      post_label_command
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("reactive-labeler-commands-sent-#{event.event_user['id']}")
    end

    private

    def any_labels_to_apply?
      labels_to_apply.any?
    end

    def labels_to_apply
      @labels_to_apply ||= command.args(event).select { |label| label.match?(ALLOWED_LABELS_REGEX) }
    end

    def command_labels
      labels_to_apply.map { |label| %Q(~"#{label}") }.join(' ')
    end

    def post_label_command
      add_comment <<~MARKDOWN.chomp
        /label #{command_labels}
      MARKDOWN
    end
  end
end

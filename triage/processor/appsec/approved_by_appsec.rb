# frozen_string_literal: true

require_relative '../appsec_processor'
require_relative '../ping_appsec_on_approval'

module Triage
  class ApprovedByAppSec < AppSecProcessor
    react_to_approvals

    def applicable?
      super && approved_by_appsec?
    end

    def process
      if appsec_discussion_thread
        discussion_id = appsec_discussion_thread['id']

        append_discussion(message, discussion_id)
      else
        add_discussion(message)

        discussion_id = appsec_discussion_thread['id']
      end

      resolve_discussion(discussion_id)
    end

    private

    def message
      @message ||= unique_comment.wrap(<<~MARKDOWN.strip)
        :white_check_mark: AppSec approved for #{event.last_commit_sha}
        by `@#{event.event_actor_username}`
      MARKDOWN
    end
  end
end

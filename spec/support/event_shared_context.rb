require_relative '../../triage/triage/user'

def stubbed_event(event_class: nil, event_user_username: nil, event_user_id: nil, label_names: [], added_label_names: [], payload: {}, **event_attrs)
  event_class ||= 'Triage::Event'
  object_kind = event_attrs[:object_kind] ||= 'issue'
  event_user_username ||= 'root'
  event_user_id ||= 1
  event_user = Triage::User.new(id: event_user_id, username: event_user_username)
  resource_author_id ||= 42
  resource_author = Triage::User.new(id: resource_author_id, username: 'joe')

  instance_double(event_class, {
    action: 'open',
    event_user: event_user,
    event_actor: event_user,
    event_actor_username: event_user_username,
    event_actor_id: event_user_id,
    event_user_username: event_user_username,
    resource_author_id: resource_author_id,
    resource_author: resource_author,
    key: "#{object_kind}.open",
    project_id: project_id,
    iid: iid,
    noteable_path: "/projects/#{project_id}/#{object_kind}s/#{iid}",
    label_names: label_names,
    added_label_names: added_label_names,
    resource_open?: true,
    url: "https://gitlab.com/group/project/-/#{object_kind}/42",
    payload: payload
  }.merge(event_attrs))
end

RSpec.shared_context 'with event' do |event_class = nil|
  let(:project_id) { 123 }
  let(:iid) { 456 }
  let(:event_user_username) { nil }
  let(:label_names) { [] }
  let(:added_label_names) { [] }
  let(:event_attrs) { {} }
  let(:payload) { {} }
  let(:event) { stubbed_event(event_class: event_class, event_user_username: event_user_username, label_names: label_names, added_label_names: added_label_names, payload: payload, **event_attrs) }
end

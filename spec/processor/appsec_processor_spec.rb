# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/appsec_processor'

RSpec.describe Triage::AppSecProcessor do
  let(:from_gitlab_org) { nil }
  let(:jihu_contributor) { nil }

  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: from_gitlab_org,
        jihu_contributor?: jihu_contributor
      }
    end
  end

  subject { described_class.new(event) }

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org' do
      let(:from_gitlab_org) { true }

      context 'but is not a JiHu contribution' do
        let(:jihu_contributor) { false }

        include_examples 'event is not applicable'
      end

      context 'and it has ~"JiHu contribution" label' do
        let(:label_names) { 'JiHu contribution' }

        include_examples 'event is applicable'
      end

      context 'and is from a JiHu contributor' do
        let(:jihu_contributor) { true }

        include_examples 'event is applicable'
      end
    end
  end
end
